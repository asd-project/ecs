//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <entt/entity/registry.hpp>
#include <entt/entity/observer.hpp>

#include <meta/types.h>
#include <meta/tuple/contains.h>

//---------------------------------------------------------------------------

namespace asd::ecs
{
    template <class T>
    concept system = requires (T v) {
        { T::tags };
    };

    namespace tags
    {
        constexpr auto active = meta::type_v<struct active_tag>;
    }

    template <class System>
    constexpr bool has_tag(auto tag) {
        return meta::contains(System::tags, tag);
    }

    template <class T>
    concept active_system = ecs::system<T> && ecs::has_tag<T>(tags::active);
}
