//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <algorithm>

#include <meta/types.h>
#include <meta/tuple/unpack.h>

#include <entt/entity/registry.hpp>

//---------------------------------------------------------------------------

namespace asd::ecs
{
    class ref
    {
    public:
        ref(entt::entity entity, entt::registry * registry) noexcept :
            _entity(entity),
            _registry(registry)
        {}

        ref(const ref & m) noexcept :
            _entity(m._entity),
            _registry(m._registry)
        {}

        ref & operator = (const ref & m) noexcept {
            _entity = m._entity;
            _registry = m._registry;

            return *this;
        }

        operator entt::entity () const {
            return _entity;
        }

        template <class Component>
        decltype(auto) operator [](meta::type<Component>) const noexcept {
            return _registry->get<const Component>(_entity);
        }

        template <class ... Components>
        auto operator ()(meta::type<Components>...) const noexcept {
            return _registry->get<const Components...>(_entity);
        }

        template <class ... Components>
        decltype(auto) get() const noexcept {
            return _registry->get<const Components...>(_entity);
        }

        template <class ... Components>
        bool any_of() const noexcept {
            return _registry->any_of<Components...>(_entity);
        }

        template <class ... Components>
        bool all_of() const noexcept {
            return _registry->all_of<Components...>(_entity);
        }
        
        template <class ... Components>
        bool any_of(meta::type<Components>...) const noexcept {
            return _registry->any_of<Components...>(_entity);
        }

        template <class ... Components>
        bool all_of(meta::type<Components>...) const noexcept {
            return _registry->all_of<Components...>(_entity);
        }
        
        template <class Component, class ... A>
        decltype(auto) emplace(A && ... args) const noexcept {
            return _registry->emplace<Component>(_entity, std::forward<A>(args)...);
        }

        template <class ... Components>
        auto emplace(meta::type<Components>...) const noexcept {
            return std::forward_as_tuple(_registry->emplace<Components>(_entity)...);
        }
        
        template <class Component, class ... A>
        decltype(auto) emplace_or_replace(A && ... args) const noexcept {
            return _registry->emplace_or_replace<Component>(_entity, std::forward<A>(args)...);
        }
        
        template <class ... Components>
        auto emplace_or_replace(meta::type<Components>...) const noexcept {
            return std::forward_as_tuple(_registry->emplace_or_replace<Components>(_entity)...);
        }
        
        template <class Component, class F>
            requires (std::invocable<F &&, Component &>)
        auto patch(F && callback) const noexcept {
            _registry->patch<Component>(_entity, std::forward<F>(callback));
        }
        
        template <class Component, class F>
            requires (std::invocable<F &&, Component &>)
        auto patch(meta::type<Component>, F && callback) const noexcept {
            patch<Component>(std::forward<F>(callback));
        }
        
        template <class ... Components, class F>
            requires (sizeof...(Components) > 1 && std::invocable<F &&, Components & ...> && !std::is_convertible_v<decltype(std::declval<F>()(std::declval<Components &>()...)), bool>)
        auto patch(F && callback) const noexcept {
            meta::unpack(_registry->get<Components...>(_entity), std::forward<F>(callback));
            patch<Components...>();
        }
        
        template <class ... Components, class F>
            requires (sizeof...(Components) > 1 && std::invocable<F &&, Components & ...> && std::is_convertible_v<decltype(std::declval<F>()(std::declval<Components &>()...)), bool>)
        auto patch(F && callback) const noexcept {
            if (meta::unpack(_registry->get<Components...>(_entity), std::forward<F>(callback))) {
                patch<Components...>();
            }
        }
        
        template <class ... Components, class F>
            requires (sizeof...(Components) > 1 && std::invocable<F &&, Components & ...>)
        auto patch(meta::types<Components...>, F && callback) const noexcept {
            patch<Components...>(std::forward<F>(callback));
        }
        
        template <class ... Components>
        auto patch() const noexcept {
            (..., _registry->patch<Components>(_entity));
        }
        
        template <class ... Components>
        auto patch(meta::types<Components...>) const noexcept {
            patch<Components...>();
        }

        template <class Component>
        void update(Component && component) const noexcept {
            _registry->replace<plain<Component>>(_entity, std::forward<Component>(component));
        }

        template <class Component>
        void erase() const noexcept {
            _registry->erase<Component>(_entity);
        }

        bool valid() const noexcept {
            return _registry != nullptr;
        }

    protected:
        entt::entity _entity;
        entt::registry * _registry = nullptr;
    };

    class handle : public ref
    {
    public:
        explicit handle(entt::registry & registry) noexcept :
            ref(registry.create(), &registry)
        {}

        handle(handle && m) noexcept :
            ref(m._entity, std::exchange(m._registry, nullptr))
        {}

        ~handle() {
            if (_registry) {
                _registry->destroy(_entity);
            }
        }

        handle & operator = (handle && m) noexcept {
            std::swap(_entity, m._entity);
            std::swap(_registry, m._registry);

            return *this;
        }
    };
}
