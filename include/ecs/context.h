//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/tuple/for_each.h>

#include <ecs/entity.h>
#include <ecs/system.h>

//---------------------------------------------------------------------------

namespace asd::ecs
{
    template <class SystemsTypes>
    struct context;

    template <class ... Systems>
    struct context<meta::types<Systems...>>
    {
        static_assert((ecs::system<Systems> && ...));

        static constexpr auto system_types = meta::types_v<Systems...>;

        context(entt::registry & registry, Systems &... systems) :
            registry(registry), systems{systems...} {}

        operator entt::registry & () {
            return registry;
        }

        template <class System>
        auto & operator () (meta::type<System>) {
            return get<System &>(systems);
        }

        template <class System>
        const auto & operator () (meta::type<System>) const {
            return get<System &>(systems);
        }

        entt::registry & registry;
        std::tuple<Systems &...> systems;
    };

    template <auto SystemsTypes>
    using context_of = context<plaintype(SystemsTypes)>;

    template <class SystemsTypes>
    void update(ecs::context<SystemsTypes> & ctx) {
        meta::for_each(ctx.systems, [&](auto & system) {
            if constexpr (ecs::active_system<plaintype(system)>) {
                system.update(ctx);
            }
        });
    }
}
